declare i32 @read(i32, i8* nocapture, i32) nounwind
declare i32 @printf(i8* noalias nocapture, ...) nounwind

@.result_fmt = private unnamed_addr constant [8 x i8] c"0x%08X\0A\00"

@crc_table = global [256 x i32] zeroinitializer
@polynomial = constant i32 u0x04C11DB7

define i32 @main(i32 %argc, i8** %argv) {
entry:
    %buffer = alloca i8
    %buffer_addr = getelementptr i8, i8* %buffer, i32 0

    call void @generate_crc_table()
    br label %read_byte

read_byte:
    %crc = phi i32 [u0xFFFFFFFF, %entry], [%next_crc, %process_byte]

    %n = call i32 @read(i32 0, i8* %buffer_addr, i32 1)
    %zero_bytes = icmp eq i32 0, %n
    br i1 %zero_bytes, label %end, label %process_byte

process_byte:
    %byte = load i8, i8* %buffer
    %byte_ref = call i8 @reflect_i8(i8 %byte)
    %byte_ref32 = zext i8 %byte_ref to i32

    %byte_ref32_shl24 = shl i32 %byte_ref32, 24
    %crc_xor = xor i32 %crc, %byte_ref32_shl24
    %index = lshr i32 %crc_xor, 24

    %ptr = getelementptr [256 x i32], [256 x i32]* @crc_table, i32 0, i32 %index
    %remainder = load i32, i32* %ptr
    %crc_shl_8 = shl i32 %crc, 8
    %next_crc = xor i32 %crc_shl_8, %remainder

    br label %read_byte

end:
    %crc_ref = call i32 @reflect_i32(i32 %crc)    
    %crc_ref_xor = xor i32 %crc_ref, u0xFFFFFFFF

    %cast210 = getelementptr [8 x i8], [8 x i8]* @.result_fmt, i32 0, i32 0
    call i32 (i8*, ...) @printf(i8* %cast210, i32 %crc_ref_xor)
    ret i32 0
}

define void @generate_crc_table() {
entry:
    %divisor = load i32, i32* @polynomial
    br label %each_byte

each_byte:
    %dividend = phi i32 [0, %entry], [%next_dividend, %each_byte]
    %dividend_shl_24 = shl i32 %dividend, 24
    %crc_value = call i32 @compute_crc32(i32 %dividend_shl_24, i32 %divisor)

    %ptr = getelementptr [256 x i32], [256 x i32]* @crc_table, i32 0, i32 %dividend
    store i32 %crc_value, i32* %ptr

    %next_dividend = add i32 %dividend, 1
    %is_dividend_ult_256 = icmp ult i32 %next_dividend, 256
    br i1 %is_dividend_ult_256, label %each_byte, label %fn_end

fn_end:
    ret void
}

define i32 @compute_crc32(i32 %init_acc, i32 %divisor) {
entry:
    br label %each_bit
    
each_bit:
    %acc = phi i32 [%init_acc, %entry], [%next_acc, %each_bit_end]
    %bit = phi i8 [0, %entry], [%next_bit, %each_bit_end]

    %msb = and i32 %acc, u0x80000000
    %is_msb_set = icmp ne i32 %msb, 0
    br i1 %is_msb_set, label %apply_xor, label %no_xor

apply_xor:
    %shl_acc.0 = shl i32 %acc, 1
    %next_acc.0 = xor i32 %shl_acc.0, %divisor
    br label %each_bit_end

no_xor:
    %next_acc.1 = shl i32 %acc, 1
    br label %each_bit_end

each_bit_end:
    %next_acc = phi i32 [%next_acc.0, %apply_xor], [%next_acc.1, %no_xor]
    %next_bit = add i8 %bit, 1

    %is_next_bit_ult_8 = icmp ult i8 %next_bit, 8
    br i1 %is_next_bit_ult_8, label %each_bit, label %fn_end

fn_end:
    ret i32 %next_acc
}

; http://graphics.stanford.edu/~seander/bithacks.html#ReverseByteWith64BitsDiv
; (b * 0x0202020202ULL & 0x010884422010ULL) % 1023;
define i8 @reflect_i8(i8 %original) {
    %original64 = zext i8 %original to i64

    %fan_out = mul i64 %original64, u0x0202020202
    %masked = and i64 %fan_out, u0x010884422010
    %result = urem i64 %masked, 1023

    %result8 = trunc i64 %result to i8
    ret i8 %result8
}

; http://graphics.stanford.edu/~seander/bithacks.html#ReverseParallel
define i32 @reflect_i32(i32 %original) {
    %swap_1 = call i32 @swap(i32 %original, i32 u0x55555555, i32 1)
    %swap_2 = call i32 @swap(i32 %swap_1, i32 u0x33333333, i32 2)
    %swap_3 = call i32 @swap(i32 %swap_2, i32 u0x0F0F0F0F, i32 4)
    %swap_4 = call i32 @swap(i32 %swap_3, i32 u0x00FF00FF, i32 8)
    %swap_5 = call i32 @swap(i32 %swap_4, i32 u0xFFFFFFFF, i32 16)
    
    ret i32 %swap_5
}

define i32 @swap(i32 %value, i32 %imm, i32 %shift) {
    %value_lshr = lshr i32 %value, %shift
    %value_lshr_masked = and i32 %value_lshr, %imm

    %value_masked = and i32 %value, %imm
    %value_masked_shl = shl i32 %value_masked, %shift

    %result = xor i32 %value_lshr_masked, %value_masked_shl
    ret i32 %result
}